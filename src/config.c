#include "config.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct app_config appConfig = {
    .enableCalendar = 1,
    .enableMpris = 1,
    .nightscoutApiToken = NULL,
    .nightscoutApiUrl = NULL,
};

int parse_boolean(const char *restrict value) {
  if (strcmp(value, "true") == 0) {
    return 1;
  }

  return 0;
}

long strim(char *s) {
  while (isspace(*s)) {
    s++;
  }

  long len = strlen(s);

  if (*s == '\0') {
    return len;
  }

  char *b = s + len - 1;
  int i = 0;
  while (isspace(*(b - i))) {
    i++;
  }

  *(b - i + 1) = '\0';

  return len - i;
}

int loadConfig() {
  size_t size;

  int home_on_heap = 0;
  int xdg_config_home_on_heap = 0;

  char *xdg_config_home = getenv("XDG_CONFIG_HOME");
  if (xdg_config_home == NULL) {
    char *home = getenv("HOME");
    if (home == NULL) {
      char *user = getenv("USER");
      if (user == NULL) {
        return 1;
      }

      size = snprintf(NULL, 0, "/home/%s", user) + 1;
      home = malloc(size);
      if (home == NULL) {
        return -1;
      }

      home_on_heap++;

      snprintf(home, size, "/home/%s", user);
    }

    size = snprintf(NULL, 0, "%s/.config", home) + 1;
    xdg_config_home = malloc(size);
    if (xdg_config_home == NULL) {
      return -1;
    }

    xdg_config_home_on_heap++;

    snprintf(xdg_config_home, size, "%s/.config", home);
    if (home_on_heap) {
      free(home);
    }
  }

  size =
      snprintf(NULL, 0, "%s/sway-status-command/config", xdg_config_home) + 1;
  char *config_path = malloc(size);
  if (config_path == NULL) {
    return -1;
  }

  snprintf(config_path, size, "%s/sway-status-command/config", xdg_config_home);

  if (xdg_config_home_on_heap) {
    free(xdg_config_home);
  }

  FILE *config_file = fopen(config_path, "r");
  free(config_path);
  if (config_file == NULL) {
    return -1;
  }

  fseek(config_file, 0, SEEK_END);
  size = ftell(config_file);
  fseek(config_file, 0, SEEK_SET);

  char *config_file_contents = malloc(size);
  if (config_file_contents == NULL) {
    return -1;
  }

  if (fread(config_file_contents, sizeof(char), size, config_file) != size &&
      ferror(config_file)) {
    return 2;
  }

  fclose(config_file);

  char *key = strtok(config_file_contents, "=");
  while (key != NULL) {
    strim(key);

    char *value = strtok(NULL, "\n");
    if (value == NULL) {
      break;
    }
    long value_size = strim(value) + 1;

    if (strcmp(key, "enable-calendar") == 0) {
      appConfig.enableCalendar = parse_boolean(value);
    } else if (strcmp(key, "enable-mpris") == 0) {
      appConfig.enableMpris = parse_boolean(value);
    } else if (strcmp(key, "nightscout-api-url") == 0) {
      appConfig.nightscoutApiUrl = malloc(value_size);
      if (appConfig.nightscoutApiUrl == NULL) {
        return -1;
      }

      strncpy(appConfig.nightscoutApiUrl, value, value_size);
    } else if (strcmp(key, "nightscout-api-token") == 0) {
      appConfig.nightscoutApiToken = malloc(value_size);
      if (appConfig.nightscoutApiToken == NULL) {
        return -1;
      }

      strncpy(appConfig.nightscoutApiToken, value, value_size);
    }

    key = strtok(NULL, "=");
  }

  free(config_file_contents);
  return 0;
}
