#include "mpris.h"
#include "config.h"
#include "log.h"
#include "status_printer.h"
#include "util.h"
#include <gio/gio.h>
#include <glib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

struct media_player_data mediaPlayerData;

void handle_mpris_properties(GDBusConnection *bus, GVariant *properties_dict);
void fetch_mpris_properties_and_insert(GDBusConnection *bus);

void handle_mpris_properties(GDBusConnection *bus, GVariant *properties_dict) {
  GVariant *playback_status_variant = g_variant_lookup_value(
      properties_dict, "PlaybackStatus", G_VARIANT_TYPE_STRING);

  GVariant *position_variant =
      g_variant_lookup_value(properties_dict, "Position", G_VARIANT_TYPE_INT64);

  GVariant *playback_rate_variant =
      g_variant_lookup_value(properties_dict, "Rate", G_VARIANT_TYPE_DOUBLE);

  GVariant *volume_variant =
      g_variant_lookup_value(properties_dict, "Volume", G_VARIANT_TYPE_DOUBLE);

  GVariant *metadata_variant = g_variant_lookup_value(
      properties_dict, "Metadata", G_VARIANT_TYPE_VARDICT);

  if (playback_status_variant != NULL) {
    gsize size = 7;
    const gchar *status = g_variant_get_string(playback_status_variant, &size);

    if (strcmp(status, "Paused") == 0) {
      mediaPlayerData.playbackStatus = PLAYBACK_STATUS_PAUSED;
    } else if (strcmp(status, "Playing") == 0) {
      mediaPlayerData.playbackStatus = PLAYBACK_STATUS_PLAYING;
    } else if (strcmp(status, "Stopped") == 0) {
      mediaPlayerData.playbackStatus = PLAYBACK_STATUS_STOPPED;
    }
    g_variant_unref(playback_status_variant);
  }

  if (position_variant != NULL) {
    mediaPlayerData.position = g_variant_get_int64(position_variant);
    g_variant_unref(position_variant);
  }

  if (playback_rate_variant != NULL) {
    mediaPlayerData.playbackRate = g_variant_get_double(playback_rate_variant);
    g_variant_unref(playback_rate_variant);
  }

  if (volume_variant != NULL) {
    mediaPlayerData.volume = g_variant_get_double(volume_variant);
    g_variant_unref(volume_variant);
  }

  if (metadata_variant != NULL) {
    GVariant *trackid_variant = g_variant_lookup_value(
        metadata_variant, "mpris:trackid", G_VARIANT_TYPE_OBJECT_PATH);

    GVariant *length_variant = g_variant_lookup_value(
        metadata_variant, "mpris:length", G_VARIANT_TYPE_INT64);

    GVariant *album_variant = g_variant_lookup_value(
        metadata_variant, "xesam:album", G_VARIANT_TYPE_STRING);

    GVariant *title_variant = g_variant_lookup_value(
        metadata_variant, "xesam:title", G_VARIANT_TYPE_STRING);

    GVariant *artist_variant = g_variant_lookup_value(
        metadata_variant, "xesam:artist", G_VARIANT_TYPE_STRING_ARRAY);

    g_variant_unref(metadata_variant);

    if (trackid_variant != NULL) {
      gchar *object_path;
      g_variant_get(trackid_variant, "o", &object_path);

      mediaPlayerData.position = 0;

      if (strcmp(object_path, MPRIS_NOTRACK_OBJECT_PATH) == 0) {
        mediaPlayerData.loaded = FALSE;
        if (bus != NULL) {
          fetch_mpris_properties_and_insert(bus);
        }
      }

      if (object_path != NULL) {
        g_free(object_path);
      }

      g_variant_unref(trackid_variant);
    }

    if (length_variant != NULL) {
      mediaPlayerData.metadata->lengthInUs =
          g_variant_get_int64(length_variant);
      g_variant_unref(length_variant);
    }

    if (album_variant != NULL) {
      gsize length;
      const gchar *album = g_variant_get_string(album_variant, &length);

      mediaPlayerData.metadata->album = escapeDoubleQuotes(album, length, NULL);

      g_variant_unref(album_variant);
    }

    if (title_variant != NULL) {
      gsize length;
      const gchar *title = g_variant_get_string(title_variant, &length);

      mediaPlayerData.metadata->title = escapeDoubleQuotes(title, length, NULL);
      g_variant_unref(title_variant);
    }

    if (artist_variant != NULL) {
      GVariantIter *iter = g_variant_iter_new(artist_variant);
      gsize size = g_variant_iter_n_children(iter);

      gsize artists_size = 1;
      mediaPlayerData.metadata->artists = g_malloc0(artists_size);
      gchar *artist_name;

      for (int i = 0; g_variant_iter_loop(iter, "s", &artist_name); i++) {
        if (artist_name == NULL || *artist_name == '\0') {
          continue;
        }

        gsize artist_name_length = strlen(artist_name);
        artist_name = escapeDoubleQuotes(artist_name, artist_name_length,
                                         &artist_name_length);
        if (i != size - 1) {
          artist_name_length += 2;
          artist_name = g_realloc(artist_name, artist_name_length);
          strcat(artist_name, ", ");
        }

        mediaPlayerData.metadata->artists =
            g_realloc(mediaPlayerData.metadata->artists,
                      artists_size + artist_name_length);

        strcat(mediaPlayerData.metadata->artists, artist_name);
      }

      g_free(iter);
    }
  }
  mediaPlayerData.loaded = TRUE;
  updateStatus();
}

void fetch_mpris_properties_and_insert(GDBusConnection *bus) {
  mediaPlayerData.loaded = FALSE;
  GError *error = NULL;
  GVariant *bus_names_variant = g_dbus_connection_call_sync(
      bus, DBUS_BUS_NAME, DBUS_OBJECT_PATH, DBUS_INTERFACE, "ListNames", NULL,
      g_variant_type_new("(as)"), G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);

  if (error != NULL) {
    SWAY_STATUS_COMMAND_LOGF(
        "unable to list dbus bus names for media player info support: %s",
        error->message);
    g_error_free(error);
    return;
  }

  GVariantIter *bus_names_iter;
  g_variant_get(bus_names_variant, "(as)", &bus_names_iter);
  g_variant_unref(bus_names_variant);

  gchar *bus_name;
  while (g_variant_iter_loop(bus_names_iter, "s", &bus_name)) {
    if (strncmp(bus_name, MPRIS_BUS_NAME_PREFIX,
                sizeof(MPRIS_BUS_NAME_PREFIX) - 1) == 0) {
      g_variant_iter_free(bus_names_iter);

      GVariant *return_values = g_dbus_connection_call_sync(
          bus, bus_name, MPRIS_OBJECT_PATH, DBUS_PROPERTIES_INTERFACE, "GetAll",
          g_variant_new("(s)", MPRIS_PLAYER_INTERFACE),
          g_variant_type_new("(a{sv})"), G_DBUS_CALL_FLAGS_NONE, -1, NULL,
          &error);

      if (error != NULL) {
        SWAY_STATUS_COMMAND_LOGF(
            "unable to receive properties of media player: %s", error->message);
        g_error_free(error);
        return;
      }

      GVariant *properties_dict;
      g_variant_get(return_values, "(@a{sv})", &properties_dict);
      g_variant_unref(return_values);
      handle_mpris_properties(NULL, properties_dict);
      return;
    }
  }
}
void __attribute__((
    optimize("O0") /* don't allow `g_variant_get` to be inlined */))
signal_get_interface_params(GVariant *parameters, gchar **interface,
                            GVariant **properties) {
  g_variant_get(parameters, "(s@a{sv}as)", interface, properties);
}

void mpris_signal_callback(GDBusConnection *bus, const gchar *sender,
                           const gchar *object_path,
                           const gchar *interface_name,
                           const gchar *signal_name, GVariant *parameters,
                           gpointer user_data) {
  if (strcmp(object_path, MPRIS_OBJECT_PATH) != 0) {
    return;
  }

  if (strcmp(interface_name, MPRIS_PLAYER_INTERFACE) == 0 &&
      strcmp(signal_name, MPRIS_PLAYER_SEEKED_SIGNAL) == 0) {
    g_variant_get(parameters, "(x)", &mediaPlayerData.position);
    return;
  }

  if (strcmp(interface_name, DBUS_PROPERTIES_INTERFACE) == 0 &&
      strcmp(signal_name, DBUS_PROPERTIES_CHANGED_SIGNAL) == 0) {
    gchar *interface = NULL;
    GVariant *properties_dict = NULL;

    signal_get_interface_params(parameters, &interface, &properties_dict);

    if (strcmp(interface, MPRIS_PLAYER_INTERFACE) != 0) {
      return;
    }

    handle_mpris_properties(bus, properties_dict);
    g_variant_unref(properties_dict);
    g_free(interface);
  }
}

void runMprisGlibLoop() {
  if (!appConfig.enableMpris) {
    return;
  }

  mediaPlayerData.position = 0;
  mediaPlayerData.playbackRate = 1.0;
  mediaPlayerData.volume = -1;
  mediaPlayerData.playbackStatus = PLAYBACK_STATUS_STOPPED;
  mediaPlayerData.metadata =
      g_malloc0(sizeof(struct media_player_data_metadata));
  mediaPlayerData.loaded = FALSE;

  GError *error = NULL;
  GDBusConnection *bus = g_bus_get_sync(G_BUS_TYPE_SESSION, NULL, &error);
  if (error != NULL) {
    SWAY_STATUS_COMMAND_LOGF(
        "unable to connect to dbus for media player info support: %s",
        error->message);
    g_error_free(error);
    return;
  }

  GMainLoop *loop = g_main_loop_new(NULL, FALSE);

  fetch_mpris_properties_and_insert(bus);

  g_dbus_connection_signal_subscribe(bus, NULL, NULL, NULL, MPRIS_OBJECT_PATH,
                                     NULL, G_DBUS_SIGNAL_FLAGS_NONE,
                                     mpris_signal_callback, NULL, NULL);

  g_main_loop_run(loop);
  g_main_loop_unref(loop);
}
