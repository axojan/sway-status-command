#include "status_title.h"
#include "i3ipc.h"
#include "util.h"
#include <stdlib.h>
#include <string.h>

char *status_title = NULL;

int updateStatusTitle() {
  I3ipc_reply_tree *tree = i3ipc_get_tree();
  I3ipc_node *node = i3ipc_find_focused_node(&tree->root);

  if (node != NULL && node->name != NULL) {
    status_title = escapeDoubleQuotes(node->name, node->name_size, NULL);
    return 0;
  }

  return -1;
}
