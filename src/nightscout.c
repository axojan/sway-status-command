#include "nightscout.h"
#include "config.h"
#include "log.h"
#include "status_printer.h"
#include <curl/curl.h>
#include <curl/easy.h>
#include <errno.h>
#include <json-c/json.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

struct nightscout_entry_data nightscoutEntryData = {
    .sgv = -1,
    .delta = 0,
    .direction = NIGHTSCOUT_ENTRY_DATA_DIRECTION_FLAT,
    .outdated = 0,
};

struct curl_writedata_cb_response {
  char *data;
  size_t size;
};

static size_t curl_writedata_cb(void *contents, size_t size, size_t nmemb,
                                void *userp) {
  size_t realsize = size * nmemb;
  struct curl_writedata_cb_response *response =
      (struct curl_writedata_cb_response *)userp;

  char *ptr = realloc(response->data, response->size + realsize + 1);
  if (ptr == NULL) {
    return 0;
  }

  response->data = ptr;
  memcpy(response->data + response->size, contents, realsize);
  response->size += realsize;
  response->data[response->size] = '\0';

  return realsize;
}

unsigned long clamp(long num, unsigned long min, unsigned long max) {
  const unsigned long t = num < min ? min : num;
  return t > max ? max : t;
}

void *nightscout_fetch_loop_cb(void *data) {
  if (appConfig.nightscoutApiUrl == NULL ||
      appConfig.nightscoutApiToken == NULL ||
      *appConfig.nightscoutApiUrl == '\0' ||
      *appConfig.nightscoutApiToken == '\0') {
    return NULL;
  }

  CURL *hnd = curl_easy_init();
  if (hnd == NULL) {
    SWAY_STATUS_COMMAND_LOG("something went wrong, initializing curl failed");
    return NULL;
  }

  size_t size =
      snprintf(NULL, 0, "%s/entries/sgv.json\?count=1&token=%s",
               appConfig.nightscoutApiUrl, appConfig.nightscoutApiToken) +
      1;

  char *url = malloc(size);
  if (url == NULL) {
    SWAY_STATUS_COMMAND_LOG("unable to allocate memory for URL: out of memory");
    return NULL;
  }

  snprintf(url, size, "%s/entries/sgv.json?count=1&token=%s",
           appConfig.nightscoutApiUrl, appConfig.nightscoutApiToken);

  curl_easy_setopt(hnd, CURLOPT_URL, url);
  free(url);

  struct curl_writedata_cb_response response = {.data = NULL, .size = 0};
  curl_easy_setopt(hnd, CURLOPT_WRITEFUNCTION, curl_writedata_cb);
  curl_easy_setopt(hnd, CURLOPT_WRITEDATA, &response);
  curl_easy_setopt(hnd, CURLOPT_FOLLOWLOCATION, 1L);

  json_tokener *tok = json_tokener_new();

  while (1) {
    unsigned long time_to_sleep = NIGHTSCOUT_FETCH_INTERVAL;

    int failed = 1;
    uint64_t date = 0;

    CURLcode ret = curl_easy_perform(hnd);
    if (ret != CURLE_OK) {
      SWAY_STATUS_COMMAND_LOGF("failed to perform request, curl error: %d",
                               ret);
    } else {
      long response_code;
      curl_easy_getinfo(hnd, CURLINFO_RESPONSE_CODE, &response_code);

      if (response_code != 200) {
        SWAY_STATUS_COMMAND_LOGF("expected response code 200, found %ld",
                                 response_code);
        free(response.data);
        response.data = NULL;
        response.size = 0;
      } else {
        struct json_object *entries_json_object =
            json_tokener_parse_ex(tok, response.data, response.size);

        free(response.data);
        response.data = NULL;
        response.size = 0;

        enum json_type entries_json_object_type =
            json_object_get_type(entries_json_object);

        if (entries_json_object_type != json_type_array) {
          SWAY_STATUS_COMMAND_LOGF(
              "entries value returned is not a JSON array, "
              "is instead %d `enum json_type`",
              entries_json_object_type);
        } else {
          struct json_object *entry_json_object =
              json_object_array_get_idx(entries_json_object, 0);

          enum json_type entry_json_object_type =
              json_object_get_type(entry_json_object);
          if (entry_json_object_type != json_type_object) {
            SWAY_STATUS_COMMAND_LOGF(
                "entry value in entries array is not a JSON object, is instead "
                "%d `enum json_type`",
                entry_json_object_type);
          } else {
            struct json_object *sgv_json_object;
            struct json_object *delta_json_object;
            struct json_object *direction_json_object;
            struct json_object *date_json_object;

            json_object_object_get_ex(entry_json_object, "sgv",
                                      &sgv_json_object);
            json_object_object_get_ex(entry_json_object, "delta",
                                      &delta_json_object);
            json_object_object_get_ex(entry_json_object, "direction",
                                      &direction_json_object);
            json_object_object_get_ex(entry_json_object, "date",
                                      &date_json_object);

            errno = 0;
            int64_t sgv = json_object_get_int64(sgv_json_object);
            if (errno != 0) {
              SWAY_STATUS_COMMAND_LOG("unable to receive sgv value; not int64");
              nightscoutEntryData.sgv = -1;
            } else {
              nightscoutEntryData.sgv = sgv;
            }

            errno = 0;
            double delta = json_object_get_double(delta_json_object);
            if (errno != 0) {
              SWAY_STATUS_COMMAND_LOG(
                  "unable to receive delta value; not int64");
            } else {
              nightscoutEntryData.delta = delta;
            }

            errno = 0;
            date = json_object_get_uint64(date_json_object);
            if (errno != 0) {
              SWAY_STATUS_COMMAND_LOG(
                  "unable to receive date value; not uint64");
              date = 0;
            }

            const char *direction =
                json_object_get_string(direction_json_object);
            if (direction == NULL) {
            } else if (strcmp(direction, "Flat") == 0) {
              nightscoutEntryData.direction =
                  NIGHTSCOUT_ENTRY_DATA_DIRECTION_FLAT;
            } else if (strcmp(direction, "FortyFiveDown") == 0) {
              nightscoutEntryData.direction =
                  NIGHTSCOUT_ENTRY_DATA_DIRECTION_FORTY_FIVE_DOWN;
            } else if (strcmp(direction, "SingleDown") == 0) {
              nightscoutEntryData.direction =
                  NIGHTSCOUT_ENTRY_DATA_DIRECTION_SINGLE_DOWN;
            } else if (strcmp(direction, "DoubleDown") == 0) {
              nightscoutEntryData.direction =
                  NIGHTSCOUT_ENTRY_DATA_DIRECTION_DOUBLE_DOWN;
            } else if (strcmp(direction, "TripleDown") == 0) {
              nightscoutEntryData.direction =
                  NIGHTSCOUT_ENTRY_DATA_DIRECTION_TRIPLE_DOWN;
            } else if (strcmp(direction, "FortyFiveUp") == 0) {
              nightscoutEntryData.direction =
                  NIGHTSCOUT_ENTRY_DATA_DIRECTION_FORTY_FIVE_UP;
            } else if (strcmp(direction, "SingleUp") == 0) {
              nightscoutEntryData.direction =
                  NIGHTSCOUT_ENTRY_DATA_DIRECTION_SINGLE_UP;
            } else if (strcmp(direction, "DoubleUp") == 0) {
              nightscoutEntryData.direction =
                  NIGHTSCOUT_ENTRY_DATA_DIRECTION_DOUBLE_UP;
            } else if (strcmp(direction, "TripleUp") == 0) {
              nightscoutEntryData.direction =
                  NIGHTSCOUT_ENTRY_DATA_DIRECTION_TRIPLE_UP;
            } else {
              nightscoutEntryData.direction =
                  NIGHTSCOUT_ENTRY_DATA_DIRECTION_UNKNOWN;
            }

            failed = 0;
          }
        }

        json_object_put(entries_json_object);
      }
    }

    long time_passed = (date / 1000) - (unsigned long)time(NULL);
    nightscoutEntryData.outdated =
        failed || time_passed > NIGHTSCOUT_FETCH_INTERVAL;

    updateStatus();

    if (date != 0 && !nightscoutEntryData.outdated) {
      time_to_sleep = clamp(time_passed + NIGHTSCOUT_FETCH_INTERVAL, 0,
                            NIGHTSCOUT_FETCH_INTERVAL);
    }

    sleep(time_to_sleep);
  }

  curl_easy_cleanup(hnd);
  json_tokener_free(tok);

  return NULL;
}
