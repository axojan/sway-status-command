#include "status_printer.h"
#include "config.h"
#include "log.h"
#include "mpris.h"
#include "nightscout.h"
#include "status_title.h"
#include <math.h>
#include <stdio.h>
#include <time.h>

const char *DISPLAY_PLAYBACK_STATUS[3] = {
    "", "", ""}; // play, pause, stop nerd font icons

const char WEEKDAYS[7][4] = {
    "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
};
const char MONTHS[12][4] = {
    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
};

const char *DISPLAY_NIGHTSCOUT_ENTRY_DIRECTION[10] = {
    "󰁔", "󰁃", "󰁅",     "󰁅󰁅",     "󰁅󰁅󰁅",
    "󰁜", "󰁝", "󰁝󰁝", "󰁝󰁝󰁝", "???"};

void updateStatus() {
  if (status_title == NULL || *status_title == '\0') {
    updateStatusTitle();
  }

  printf("[");

  if (nightscoutEntryData.sgv != -1) {
    const char *glucose_icon = nightscoutEntryData.outdated ? "" : "";

    printf("{\"name\":\"glucose_info\",\"full_text\":\"%s %ld %+.1f "
           "%s\",\"short_text\":\"%s %ld\"},",
           glucose_icon, nightscoutEntryData.sgv, nightscoutEntryData.delta,
           DISPLAY_NIGHTSCOUT_ENTRY_DIRECTION[nightscoutEntryData.direction],
           glucose_icon, nightscoutEntryData.sgv);
  }

  if (mediaPlayerData.metadata != NULL && mediaPlayerData.loaded) {
    printf("{\"name\":\"song_data\",\"full_text\":\"  ");

    if (mediaPlayerData.metadata->album != NULL &&
        *mediaPlayerData.metadata->album != '\0') {
      printf("%s | ", mediaPlayerData.metadata->album);
    }

    if (mediaPlayerData.metadata->artists != NULL &&
        *mediaPlayerData.metadata->artists != '\0') {
      printf("%s - ", mediaPlayerData.metadata->artists);
    }

    if (mediaPlayerData.metadata->title != NULL &&
        *mediaPlayerData.metadata->title != '\0') {
      printf("%s | ", mediaPlayerData.metadata->title);
    }

    gint64 positionInMin =
        (gint64)floor(mediaPlayerData.position / 1000.0 / 1000.0 / 60.0);
    gint64 positionInSec =
        (gint64)floor(mediaPlayerData.position / 1000.0 / 1000.0) % 60;
    gint64 lengthInMin = (gint64)floor(mediaPlayerData.metadata->lengthInUs /
                                       1000.0 / 1000.0 / 60.0);
    gint64 lengthInSec =
        (gint64)floor(mediaPlayerData.metadata->lengthInUs / 1000.0 / 1000.0) %
        60;

    printf("%s  %02li:%02li/%02li:%02li",
           DISPLAY_PLAYBACK_STATUS[mediaPlayerData.playbackStatus],
           positionInMin, positionInSec, lengthInMin, lengthInSec);

    if (mediaPlayerData.volume != -1) {
      printf(" |   %.2f%%", mediaPlayerData.volume);
    }

    printf("\",");

    printf("\"short_text\":\"");

    if (mediaPlayerData.metadata->title != NULL &&
        *mediaPlayerData.metadata->title != '\0') {
      printf("%s | ", mediaPlayerData.metadata->title);
    }

    printf("%s  %02li:%02li/%02li:%02li\"},",
           DISPLAY_PLAYBACK_STATUS[mediaPlayerData.playbackStatus],
           positionInMin, positionInSec, lengthInMin, lengthInSec);
  }

  if (appConfig.enableCalendar) {
    time_t utctime = time(NULL);
    if (utctime == (time_t)(-1)) {
      SWAY_STATUS_COMMAND_LOG(
          "unable to get current UTC time, something went wrong");
    } else {
      struct tm *timeinfo = localtime(&utctime);
      printf("{\"name\":\"calendar\",\"full_text\":\"󰃭 %s, %s %2d %4d "
             "%.2d:%.2d:%.2d\",\"short_text\":\"%.2d:%.2d:%.2d\"},",
             WEEKDAYS[timeinfo->tm_wday], MONTHS[timeinfo->tm_mon],
             timeinfo->tm_mday, timeinfo->tm_year + 1900, timeinfo->tm_hour,
             timeinfo->tm_min, timeinfo->tm_sec, timeinfo->tm_hour,
             timeinfo->tm_min, timeinfo->tm_sec);
    }
  }

  printf("{\"name\":\"status_title\",\"full_text\":\"%s\"}],\n", status_title);

  fflush(stdout);
}
