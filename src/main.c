#include "config.h"
#include "i3ipc.h"
#include "log.h"
#include "mpris.h"

#include "nightscout.h"

#include "status_printer.h"
#include "status_title.h"
#include <curl/curl.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

void *i3status_read_loop();
void *i3ipc_event_loop();

void *sway_events_loop_cb(void *data) {
  enum I3ipc_event_type events[2] = {
      I3IPC_EVENT_WINDOW,
      I3IPC_EVENT_WORKSPACE,
  };
  i3ipc_subscribe(events, sizeof(events) / sizeof(events[0]));

  while (1) {
    I3ipc_event *event = i3ipc_event_next(-1);
    if (event->type == I3IPC_EVENT_WINDOW ||
        event->type == I3IPC_EVENT_WORKSPACE) {
      updateStatusTitle();
      updateStatus();
    }
  }

  return NULL;
}

void *main_events_loop_cb(void *data) {
  // avoid running an unnecessary thread
  if (!appConfig.enableCalendar && !appConfig.enableMpris) {
    return NULL;
  }

  struct timespec prev_timespec;
  clock_gettime(CLOCK_MONOTONIC, &prev_timespec);

  while (1) {
    struct timespec curr_timespec;
    clock_gettime(CLOCK_MONOTONIC, &curr_timespec);
    double dt =
        ((1e6 * curr_timespec.tv_sec) -
         (1e6 * prev_timespec.tv_sec)) /* diff between secs (converted to us) */
        + ((1e-3 * curr_timespec.tv_nsec) -
           (1e-3 *
            prev_timespec.tv_nsec)) /* diff between nsecs (converted to us) */;

    prev_timespec = curr_timespec;

    if (mediaPlayerData.playbackStatus == PLAYBACK_STATUS_PLAYING) {
      mediaPlayerData.position += mediaPlayerData.playbackRate * dt;

      if (!appConfig.enableCalendar) {
        updateStatus();
      }
    }

    if (appConfig.enableCalendar) {
      updateStatus();
    }

    sleep(1);
  }

  return NULL;
}

int main(void) {
  curl_global_init(CURL_GLOBAL_NOTHING);

  // initalize sway socket
  char swaysockpath[108];
  FILE *fp = popen("sway --get-socketpath", "r");
  if (fgets(swaysockpath, 108, fp) == NULL) {
    SWAY_STATUS_COMMAND_LOG("failed to retrieve swaysock path");
    return 1;
  }
  pclose(fp);

  swaysockpath[strcspn(swaysockpath, "\r\n")] = '\0';

  i3ipc_init_try(swaysockpath);

  puts("{\"version\":1}\n[");

  int retvalue = loadConfig();
  switch (retvalue) {
  case -1:
    SWAY_STATUS_COMMAND_LOG("out of memory");
    break;
  case 1:
    SWAY_STATUS_COMMAND_LOG("unable to find XDG_CONFIG_HOME");
    break;
  case 2:
    SWAY_STATUS_COMMAND_LOG("unable to open config file");
    break;
  }

  updateStatus();

  // pthread is used here instead of the Glib loop because
  // it has problems with delays due to being busy with
  // handling other things (like listening to sway events on
  // `sway_event_listen_loop_cb` which is blocking) since it runs on the
  // same thread. More info: https://docs.gtk.org/glib/func.timeout_add.html
  pthread_t sway_events_thread, main_events_thread, nightscout_events_thread;
  pthread_create(&sway_events_thread, NULL, sway_events_loop_cb, NULL);
  pthread_create(&main_events_thread, NULL, main_events_loop_cb, NULL);
  pthread_create(&nightscout_events_thread, NULL, nightscout_fetch_loop_cb,
                 NULL);

  runMprisGlibLoop();

  pthread_join(sway_events_thread, NULL);
  pthread_join(main_events_thread, NULL);
  pthread_join(nightscout_events_thread, NULL);

  curl_global_cleanup();
  return 0;
}
