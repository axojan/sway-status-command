#include "util.h"
#include <stdlib.h>

char *escapeDoubleQuotes(const char *s, size_t length, size_t *new_length) {
  if (length == 0) {
    return "";
  }

  size_t buf_size = 1;
  char *buf = malloc(length * 2);

  int j = 0;
  for (int i = 0; i < length; i++) {
    if (s[i] == '"') {
      buf_size += 2;
      buf[j] = '\\';
      j++;
      buf[j] = s[i];
      j++;
    } else {
      buf_size++;
      buf[j] = s[i];
      j++;
    }
  }

  buf[buf_size - 1] = '\0';

  if (new_length != NULL) {
    *new_length = buf_size;
  }

  buf = realloc(buf, buf_size);
  return buf;
}
