#ifndef __SWAY_STATUS_COMMAND_UTIL_H__
#define __SWAY_STATUS_COMMAND_UTIL_H__

#include <stddef.h>

char *escapeDoubleQuotes(const char *s, size_t length, size_t *new_length);

#endif
