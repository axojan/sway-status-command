#ifndef __SWAY_STATUS_COMMAND_LOG_H__
#define __SWAY_STATUS_COMMAND_LOG_H__

#define STRINGIFY_DETAIL(x) #x
#define STRINGIFY(x) STRINGIFY_DETAIL(x)

#define SWAY_STATUS_COMMAND_LOG(s)                                             \
  fputs("[" __FILE__ ":" STRINGIFY(__LINE__) "] " s "\n", stderr)

#define SWAY_STATUS_COMMAND_LOGF(format, ...)                                  \
  fprintf(stderr,                                                              \
          "["__FILE__                                                          \
          ":" STRINGIFY(__LINE__) "] " format "\n",                            \
          __VA_ARGS__);

#endif
