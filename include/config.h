#ifndef __SWAY_STATUS_COMMAND_CONFIG_H__
#define __SWAY_STATUS_COMMAND_CONFIG_H__

struct app_config {
  int enableCalendar;
  int enableMpris;
  char *nightscoutApiToken;
  char *nightscoutApiUrl;
};

extern struct app_config appConfig;

int loadConfig();

#endif
