#ifndef __SWAY_STATUS_COMMAND_MPRIS_H__
#define __SWAY_STATUS_COMMAND_MPRIS_H__

#include <glib.h>

#define MPRIS_OBJECT_PATH "/org/mpris/MediaPlayer2"
#define MPRIS_NOTRACK_OBJECT_PATH MPRIS_OBJECT_PATH "/TrackList/NoTrack"
#define MPRIS_PLAYER_INTERFACE "org.mpris.MediaPlayer2.Player"
#define MPRIS_BUS_NAME_PREFIX "org.mpris.MediaPlayer2."
#define MPRIS_PLAYER_SEEKED_SIGNAL "Seeked"
#define DBUS_OBJECT_PATH "/org/freedesktop/DBus"
#define DBUS_PROPERTIES_INTERFACE "org.freedesktop.DBus.Properties"
#define DBUS_PROPERTIES_CHANGED_SIGNAL "PropertiesChanged"
#define DBUS_BUS_NAME "org.freedesktop.DBus"
#define DBUS_INTERFACE "org.freedesktop.DBus"

enum playback_status {
  PLAYBACK_STATUS_PLAYING,
  PLAYBACK_STATUS_PAUSED,
  PLAYBACK_STATUS_STOPPED,
};

struct media_player_data_metadata {
  gint64 lengthInUs;
  gchar *title;
  gchar *artists;
  gchar *album;
};

struct media_player_data {
  enum playback_status playbackStatus;
  gdouble playbackRate;
  gdouble volume;
  gint64 position;
  struct media_player_data_metadata *metadata;
  gboolean loaded;
};

extern struct media_player_data mediaPlayerData;

void runMprisGlibLoop();

#endif
