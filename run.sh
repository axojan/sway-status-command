#!/bin/sh

DIRNAME=`dirname "$0"`
BUILDDIR="$DIRNAME/build"
COMMAND_PATH="$BUILDDIR/SwayStatusCommand"

if [ -f $COMMAND_PATH ]; then
  $COMMAND_PATH
elif ! command -v cmake make &> /dev/null; then
  echo "please install cmake and make, $(basename "$0") does not work without them"

  while :; do
    :
  done
fi

if [ ! -d $BUILDDIR ]; then
  cmake -B build -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
fi

make -C $BUILDDIR &> /dev/null

$COMMAND_PATH
